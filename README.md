# s7plc conda recipe

Home: https://github.com/paulscherrerinstitute/s7plc

Package license: GNU General Public License v3.0

Recipe license: BSD 3-Clause

Summary: EPICS driver for communication with PLCs
